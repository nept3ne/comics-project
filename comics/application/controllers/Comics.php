<?php
    $route['c15/comics/index.php/comics/(:num)']='comics/listeComic/$1';
    class Comics extends CI_Controller{
        
        public function __construct(){
            parent::__construct();
            //chargement des helper, model, library necessaire aux comics 
            $this->load->model("Comics_model");
            $this->load->library('session');
            $this->load->library('Etat');
            //-> Etat incorpore des fonctions de verification utiles
            $this->load->library('Result');
            //-> Result incorpore des fonctions de retranscription d'etat
            $this->load->helper('url');
            $this->load->helper('form');
            $this->load->library('form_validation');
        }
        public function index(){
            //aucune variables
            //affiche l'accueil du site, 

			$data = array(
				"error"=>$this->result->inError(),
				"done"=>$this->result->inDone(),
				"data1"=>"",
				"view1"=>"accueil"
			);
			$this->load->view("template",$data);
        }
        public function listeComic($page = 1){
            //page -> facultatif, si non fournis il est remplacé par 1
            //affiche la liste de comic 10 par 10

            //recolte des informations nécessaires pour les comics, et les numerotation de la page
            $array = $this->Comics_model->getTenComics($page);
            $totalPage = (int) $this->Comics_model->getNumberOfComics()/10;
            $data = array(
                "error"=>$this->result->inError(),
                "done"=>$this->result->inDone(),
                "data1"=>array("lesComics"=>$array,"page"=>$page,"totalPage"=>$totalPage),
                "view1"=>"listeComic"
            );
            $this->load->view("template",$data);
        }

        public function connexion(){
            //login -> type : post, facultatif (sauf si une autre variable est saisie)
            //password -> type : post, facultatif (sauf si une autre variable est saisie)
            //affiche la page de connexion

            $this->form_validation->set_rules('login','login','required');
            $this->form_validation->set_rules('password','password','required');
            if ($this ->form_validation ->run() ===  TRUE){
                //nettoyage des failles de securités xss
                $login = $this->security->xss_clean($this->security->xss_clean($this->input->post('login'))); 
                $password = $this->security->xss_clean($this->input->post('password'));
                if ($this->etat->getConnected($login,$password)!=1){//getconnected permet de verifier si le mdp et login sont correcte et de les connecter
                    //cas erreur de login/mdp
                    $this->session->set_userdata("error","errLog");
                    $this->load->view("template",array(
                        "view1"=>"login",
                        "data1"=>array("error"=>$this->result->inError())));
                }
                else{
                    //cas, connecté
                    if($this->session->has_userdata("add")){
                        //si on a tenté d'ajouter sans etre connecte
                        $data = $this->session->userdata("add");
                        $this->session->unset_userdata("add");
                        redirect("user/lier/$data");
                    }
                    $this->session->set_userdata("done","conUser");
                    redirect('user/index');
                }
            }else if($this->etat->connected()==1){ 
                redirect('user/index');
            }else{
                //chargement du formulaire de connection
                $this->load->view("template",array(
                    "error"=>$this->result->inError(),
                    "view1"=>"login",
                    "data1"=>array("error"=>0)));
                $this->session->unset_userdata("error");
            }
        }
        public function addAccount(){
            //login -> type : post obligatoire
            //name -> type : post obligatoire
            //firstname -> type : post obligatoire
            //password -> type : post obligatoire
            //passwordConf -> type : post obligatoire
            // affiche la page de creation de compte

            if(!$this->etat->connected()){
                $this->form_validation->set_rules('login','login','required');
                $this->form_validation->set_rules('name','nom','required');
                $this->form_validation->set_rules('firstname','prenom','required');
                $this->form_validation->set_rules('password','password','required');
                $this->form_validation->set_rules('passwordConf','confirmation password','required');
                if($this->form_validation->run()===TRUE){
                    //suppression du xss et recuperation des variables post
                    $login = $this->security->xss_clean($this->input->post('login'));
                    $name = $this->security->xss_clean($this->input->post('name'));
                    $firstname = $this->security->xss_clean($this->input->post('firstname'));
                    if($this->Comics_model->comics_getOneCollector($login)==null){
                        $password = $this->security->xss_clean($this->input->post('password'));
                        $passwordConf = $this->security->xss_clean($this->input->post('passwordConf'));
                        if($password==$passwordConf){
                            //encryption du password, pour l'inserer dans la bdd
                            $password = $this->etat->encryption($password);
                            $this->Comics_model->addCollector($login,$name,$firstname,$password);
                            $this->etat->getConnected($login,$this->security->xss_clean($this->input->post('password')));
                            $this->session->set_userdata("done","addAccount");
                            redirect("user/index");
                        }else{
                            //cas, mot de passe non correspondant
                            $this->session->set_userdata("error","mdpincorconf");
                            $data = array(
                                "error"=>$this->result->inError(),
                                "view1"=>"creerCompte",
                                "data1"=>""
                            );
                            $this->load->view("template",$data);
                        }
                    }else{
                        //cas login déjà utilisé+
                        $this->session->set_userdata("error","doubUser");
                        $data = array(
                            "error"=>$this->result->inError(),
                            "view1"=>"creerCompte",
                            "data1"=>""
                        );
                        $this->load->view("template",$data);
                    }
                }else{
                    //cas, il n'y a pas de post
                    $data = array(
                        "view1"=>"creerCompte",
                        "data1"=>""
                    );
                    $this->load->view("template",$data);
                }
            }else{
                //cas deja connecté
                redirect("user/index");
            }
        }
    }
