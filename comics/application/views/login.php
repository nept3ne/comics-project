    <!-- Probleme, lors d'une erreur de saisie des mots de passes, le header ne fait plus sa taille original, et le titre deborde -->
    <?php
        echo validation_errors();
        echo form_open('comics/connexion');  
        echo form_fieldset('Connexion');
        echo("<div>");
        if($error!=null){
            echo'<legend style=" margin-right : 20px;">'.$error.'</legend>';
        }
    ?>
    <h2>Login</h2>
    <div class="login">
    	<div>
            <i class="fas fa-user"></i>
    		<input type="text" size="20" name="login" placeholder="login" id="login">
    	</div>
    	<div>
            <i class="fas fa-unlock-alt"></i>
    		<input type="password" size="50" name="password" placeholder="password" id="password">
    	</div>
    </div>
    
    <input type="submit" name="submitButton" value="Connexion" id="submitButton">
    <?php
        echo form_fieldset_close();
        echo form_close();
    ?>

