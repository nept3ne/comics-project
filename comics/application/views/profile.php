        <?php
            $view = "listeComic";
        ?>
        <div class="profile">
            <div>
                <div>
                    <!-- Mise en place du l'animation pour le nom -->
                    <h2>Nom :</h2>
                    <div class="name">
                        <?php
                        $array = str_split($name);
                        foreach ($array as $value) {
                            echo "<span>$value</span>";
                        }
                        ?>
                    </div>
                </div>
                <div>
                    <h2>Prenom : </h2>
                    <!-- prenom -->
                    <h2><?php echo $firstname;?></h2>
                </div>
            </div>
            <div class="remplissageCollection">
                <h2>Remplissage de la collection :</h2>
                <div>
                        <!-- Barre de remplissage de la collection -->
                    <?php
                        $size = sizeof($listeComic);
                        if($size<=5){$color = "green";}
                        else if($size<8){$color = "orange";}
                        else{$color = "red";}
                    ?>
                    <span class="<?php if(sizeof($listeComic)>=8){echo"warning";}?>" style="background-color : <?php echo $color;?>; width : <?php echo sizeof($listeComic)*10;?>%;"></span>
                </div>
            </div>
            <div>
                <a href="<?php echo base_url().'index.php/comics/listeComic/';?>"><i class="fas fa-plus-square fa-3x"></i></a>
                <a href="<?php echo base_url()."index.php/user/set";?>"><i class="fas fa-user-cog fa-3x"></i></a>
                <a href="<?php echo base_url()."index.php/user/del";?>" onclick="return suppr();"><i class="fas fa-trash-alt fa-3x"></i></a> 
                <!-- La balise onclick permet de declencher l'action de confirmation de suppression -->
            </div>
            <script>
                function suppr(){return confirm('Etes-vous sur de supprimer votre profil ?');}
            </script>
            
        </div>
        <?php if(empty($listeComic)):?>
        <div class="profile">
            <h2>Collection de comic vide </h2>
            <a href="<?php echo base_url().'index.php/comics/listeComic/';?>">
                <i class="fas fa-plus-square fa-3x"></i>    
            </a>
        </div>
        <?php
            endif;
            $this->load->view($view,array("lesComics"=>$listeComic));
        ?>
