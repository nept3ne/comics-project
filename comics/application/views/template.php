<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/header-footer.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/box.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/login.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/profile.css">

</head>
<body>
    <header>
        <a href="<?php echo base_url(); ?>index.php/comics/index" class="h1">
            <span class="char1">L</span>
            <span class="char2">e</span>
            <span class="char3">s</span>
            <span class="char4">C</span>
            <span class="char5">o</span>
            <span class="char6">m</span>
            <span class="char7">i</span>
            <span class="char8">c</span>
            <span class="char9">s</span>
        </a>
        
        <div style="">
                <?php
            echo anchor('comics/listeComic', 'Voir la liste de comics', array("class"=>"template"));
            //renommer plus clairement les autres view
            if(!$this->etat->connected()){
                echo anchor('comics/connexion', 'se connecter', array("class"=>"template"));
                echo anchor('comics/addAccount', 'creer un compte', array("class"=>"template"));
                
            }else{
                echo anchor('comics/connexion', 'mon compte', array("class"=>"template"));
                echo anchor('user/deconnexion', 'se deconnecter', array("class"=>"template"));
            }
            if($this->etat->isadmin()){
                echo anchor('admin/index', 'Panneau de controle', array("class"=>"template"));
            }
            ?>
        </div>
    </header>
    <main>
            
    <?php
    if(isset($error)):?>
        <div id="error" class="message"><?php echo "erreur : $error" ; ?></div>
    <?php endif;
    if(isset($done)):?>
        <div id="done" class="message"><?php echo "message : $done" ; ?></div>
    <?php
    endif;
    if(isset($data1)){
        $this->load->view($view1,$data1);
    }else{
        $this->load->view($view1);
    }?>
    </main>
    <footer>
        <div>
            <h3>Un site sur le thème des : </h3>
            <a href="comics/index" class="h1">
                <span class="char4">C</span>
                <span class="char5">o</span>
                <span class="char6">m</span>
                <span class="char7">i</span>
                <span class="char8">c</span>
                <span class="char9">s</span>
            </a>
        </div>

        <div>
            <h3>Un site web réalisé par : </h3>
            <ul>
                <li>Matthis Guilleray</li>
                <li>Antoine Précigout</li>
            </ul>
        </div>
    </footer>
</body>
<script src="<?php echo base_url(); ?>assets/js/hid.js"></script>
<script src="https://kit.fontawesome.com/1be028b7fd.js" crossorigin="anonymous"></script>
</html>