function hideMessage(){
    if(document.querySelector("#error")!=null || document.querySelector("#done")!=null){
        var main = document.querySelector("main");
        main.className += "messageHidden";
        var message = document.querySelector(".message");
        message.style.backgroundColor = "none";
        message.innerHTML = "";
    }
}

setTimeout(hideMessage,5000);

