<?php
        echo validation_errors();
        if($adminMode==TRUE){
            echo form_open("admin/modifyaccount/".$login);
        }else{
            echo form_open('user/set');  
        }
            echo form_fieldset('Modifier les informations du compte');
            ?>
<div class="creationCompte">
	<div>
		<label for="name">nom</label>
		<input type="text" size="30" placeholder="nom" name="name" value="<?php echo $name ;?>" id="name">
	</div>
	<div>
		<label for="firstname">prenom</label>
		<input type="text" size="30" placeholder="prenom" name="firstname" value="<?php echo $firstname ;?>" id="firstname">
	</div>
	<div>
		<label for="password">password</label>
        <input type="password" size="50" placeholder="nouveau mot de passe" name="password" value="" id="password">
    </div>
    <div>
		<label for="password">password confirmation</label>
		<input type="password" size="50" placeholder="confirmation mot de pass"name="passwordConf" value="" id="passwordConf">
	</div>
	<?php if($adminMode==FALSE):?>
	<div >
		<label for="oldpassword">ancien password</label>
		<input type="password" size="50" placeholder="ancien mot de passe" name="oldPassword" value="" id="oldPassword">
	</div>
	<?php endif;?>

	<?php if($adminMode==TRUE):?>
	<div>
		<label>Modification du statut admin</label>
		<input type="radio" name="admin" <?php if($admin=="t"){echo "checked";}?> value="1">est admin
		<input type="radio" name="admin" <?php if($admin=="f"){echo "checked";}?> value="0">n'est pas admin
	</div>
	<?php endif;?>
	
	<input type="submit" name="submitButton" value="valider" id="submitButton">
</div>
<?php
            echo form_fieldset_close();
        echo form_close();
    ?>
