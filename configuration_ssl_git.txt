# Pour eviter d'avoir un refus de connexion sur le serveur gitlab de l'IUT :
git config --global http.sslverify false


# puis faire le clone
git clone https://gitlab.iutlan.univ-rennes1.fr/mondepotgit

# remettre la sécurité globale 
git config --global http.sslverify true

# aller dans le répertoire du projet
cd mondepotgit

# enlever le verify en local
git config http.sslverify false # plus de global ici

git commit -am 'mon message de commit'

git push // ou pull
