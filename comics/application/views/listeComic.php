<div class="listeComics">
	<?php

	//Block de fleche du haut dans la liste de comic general
	//refaire un div pour les fleches droites et pour les fleches gauches
	if(isset($page)) {
		
		echo("<div>");
		
		echo("<div style=\"flex-direction : row;\">");
		//left block
		$pageRest = $page - 1;
		if ($pageRest > 0) {
			echo anchor("comics/listeComic/" . ($page - 1), "page precedente");
			if ($pageRest > 1) {
				echo anchor("comics/listeComic/" . ($page - 2), "-2");
				if ($pageRest > 2) {
					echo anchor("comics/listeComic/" . ($page - 3), "-3");
				}
			}
		}
		echo "<p>n°" . $page."</p>";
		//Right bolck
		$pageRest = $totalPage - $page;
		//echo($pageRest);
		if ($pageRest > 0) {
			if ($pageRest > 2) {
				echo anchor("comics/listeComic/" . ($page + 3), "+3");
			}
			if ($pageRest > 1) {
				echo anchor("comics/listeComic/" . ($page + 2), "+2");
			}
			echo anchor("comics/listeComic/" . ($page + 1), "page suivante");
		}
		echo("</div>");
		echo("</div>");

	}

	//affichage de la liste de comic
	//$lesComics -> array de comic (image,nom,date,n°)
    foreach ($lesComics as $comic) {
        $this->load->view("unComic",$comic);
    }




	//Block de fleche du bas, dans la liste de fleche du bas
	if(isset($page)) {
		
		echo("<div>");
		echo("<div style=\"flex-direction : row;\">");
		//left block
		$pageRest = $page - 1;
		if ($pageRest > 0) {
			echo anchor("comics/listeComic/" . ($page - 1), "page precedente");
			if ($pageRest > 1) {
				echo anchor("comics/listeComic/" . ($page - 2), "-2");
				if ($pageRest > 2) {
					echo anchor("comics/listeComic/" . ($page - 3), "-3");
				}
			}
		}
		echo "<p>n°" . $page."</p>";
		//Right bolck
		$pageRest = $totalPage - $page;
		//echo($pageRest);
		if ($pageRest > 0) {
			if ($pageRest > 1) {
				echo anchor("comics/listeComic/" . ($page + 2), "+2");
				if ($pageRest > 2) {
					echo anchor("comics/listeComic/" . ($page + 3), "+3");
				}
			}
			echo anchor("comics/listeComic/" . ($page + 1), "page suivante");
		}
		echo("</div>");
		echo("</div>");

	}
	?>
</div>
