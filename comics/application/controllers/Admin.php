<?php
    class Admin extends CI_Controller{
        public function __construct(){
            parent::__construct();
            //Chargement des librairies,models,helper necessaires a la partie admin
            $this->load->model("Comics_model");
            $this->load->library('session');
            $this->load->library('etat');
            $this->load->library('Result');
            $this->load->helper('url');
            $this->load->library('form_validation');
            //verifie si l'utilisateur est un admin à chaque chargement du controlleur
            if(!$this->etat->isadmin()==1){
                $this->session->set_userdata("error","adminError");
                redirect("user");
            }
        }
        public function index(){
            //Ne necessite aucune variable
            //Affiche la page d'accueil admin

            $data1 = $this->Comics_model->getAllCollector();
            $data = array(
                "done"=>$this->result->inDone(),
                "error"=>$this->result->inError(),
                "view1"=>"accounts",
                "data1"=>array(
                    "profil"=>$data1
                )
            );
            $this->load->view("template",$data);
        }

        public function suppr($login){
            //login -> obligatoire
            //Aucun affichage
            
            $this->Comics_model->delCollector($login);
            $this->session->set_userdata("done","delUser");
            redirect("admin");           
        }
        public function modifyAccount($login){
            //login -> obligatoire
            //name ->type :  post obligatoire, prérempli dans le formulaire
            //firstname ->type :  post obligatoire, prérempli dans le formulaire
            //password ->type :  post facultatif, non-préremplie dans le formulaire
            //passwordConf ->type : post obligatoire si password est saisie, non-préremplie dans le formulaire
            //affiche un formulaire

            $this->form_validation->set_rules('name','nom','required');
            $this->form_validation->set_rules('firstname','prenom','required');

            if($this->form_validation->run()===TRUE){//verifie quels champs sont remplis
                $arrayCollector = $this->Comics_model->comics_getOneCollector($login);
                $name = $this->security->xss_clean($this->input->post('name'));
                if($name==NULL){$name=$arrayCollector["name"];}
                    $firstname = $this->security->xss_clean($this->input->post('firstname'));
                if($firstname==NULL){$firstname=$arrayCollector["firstname"];}
                    $password = $this->security->xss_clean($this->input->post('password'));;  
                    $passwordConf = $this->security->xss_clean($this->input->post('passwordConf'));;     
                    $admin = $this->security->xss_clean($this->input->post('admin'));;  
                if($password==$passwordConf){
                    //affect les champs correspondant        
                    if($password==NULL){
                        $this->Comics_model->setCollectorExceptPass($login,$name,$firstname);
                    }else{
                        $this->Comics_model->setCollector($login,$name,$firstname,$password); 
                    }
                    $this->Comics_model->setAdmin($login,$admin);
                    $this->session->set_userdata("done","accountchanged");
                    redirect("admin");
                }else{
                    //dans le cas ou le mdp de confirmation est incorrect
                    $array["adminMode"] = TRUE;
                    $data = array(
                        "error"=>$this->result->inError("mdpincorconf"),
                        "view1"=>"modifierCompte",
                        "data1"=>$array
                    );
                    $this->load->view("template",$data);
                }
            } else{
                //affichage du formulaire
                $array = $this->Comics_model->comics_getOneCollector($login);
                $array["adminMode"] = TRUE;
                $dataTemplate = array(
                    "done"=>$this->result->inDone(),
                    "error"=>$this->result->inError(),
                    "view1"=>"modifierCompte",
                    "data1"=>$array
                );
                $this->load->view("template",$dataTemplate);
            }
        }

    }
?>