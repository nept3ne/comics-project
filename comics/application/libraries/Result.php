<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Result {
    public function __construct(){
        $this->CI =& get_instance();
        //chargement de la librarie nécessaire pour les fct
        $this->CI->load->library("session");
    }
    public function inError($error = null){
        //error -> code d'erreur, automatiquement en null, si pas passé en parametre
        //error -> type : session, facultatif prime toujours sur le parametre  
        if($this->CI->session->has_userdata("error")){//affecte à error, si la variable de session existe
            $error = $this->CI->session->userdata("error");
            $this->CI->session->unset_userdata("error");
        }
        //switch sur tous les codes d'erreurs possibles
        switch ($error) {
            case 404:
                $description = "Page non trouvée";
                break;
            case 403:
                $description = "Page interdite d'accès";
                break;
            case "adminError":
                $description = "Vous n'avez pas les droits d'administrateur pour effectuer cette action";
                break;
            case "10Coms":
                $description = "Vous avez 10 comics dans votre libraire, votre comics le plus récent à été supprimé au profit du nouveau";
                break;
            case "discUser":
                $description = "Vous n'êtes pas connecté";
                break;
            case "doubCom":
                $description = "Vous ne pouvez ajouter le comics car il est déjà dans la collection";
                break;
            case "doubUser":
                $description = "Le nom d'utilisateur est déjà pris";
                break;
            case "iderror":
                $description = "Le mot de passe entré est érroné";
                break;
            case 'mdpincorconf':
                $description = "Le mot de passe n'a pas bien été confirmé";
                break;
            case 'errLog':
                $description = "Le login et/ou le mot de passe saisie sont incorrect";
                break;
            case 'errChps':
                $description = "L'un des champs à mal été remplis";
                break;
            case null:
                $description = null;
                break;
            default:
               $description = "Inconnu : veillez contacter l'admin avec le code d'erreur suivant : $error";
                break;
        }
        return $description;
    }
    public function inDone($done = null){
        //done -> code de retour, automatiquement en null, si pas passé en parametre
        //done -> type : session, facultatif prime toujours sur le parametre          
        if($this->CI->session->has_userdata("done")){//affect à done la variable de session, si elle existe
            $done = $this->CI->session->userdata("done");
            $this->CI->session->unset_userdata("done");
        }
        //switch sur tous les code de retours positifs
        switch ($done) {
            case "comAdd":
                $description = "Votre comic à été ajouté à la collection";
                break;
            case "conUser":
                $description = "Connexion réussi";
                break;
            case "discUser":
                $description = "Vous avez bien été deconnecté";
                break;
            case "addAccount":
                $description = "Votre compte à bien été ajouté";
                break;
            case "delUser":
                $description = "L'utilisateur à bien été supprimé";
                break;
            case "addAdmin":
                $description = "Les paramètres admin ont bien été changé";
                break;
            case "delCom":
                $description = "Le comic à bien été supprimé";
                break;
            case "resPass":
                $description = "Le mot de passe à bien été reinitialisé à \"password\"";
                break;
            case "accountchanged":
                $description = "Les informations du compte ont bien été changée";
                break;
            case null:
                $description = null;
                break;
            default:
                $description = "Erreur inconnu : veillez contacter l'admin";
                break;
        }
        return $description;
    }

}