<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Etat {
    protected $CI;
	protected $encrypted=FALSE;
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->library('session');
        $this->CI->load->library('encryption');
    }
    public function verify_password($string1,$string2){
        $array = password_get_info($string2);
        $encrypted = $array["algo"];
        if($encrypted!=0){
            return password_verify($string1,$string2);
        }else{
            return $string1==$string2;
        }
    }
    public function encryption($string){
		if($this->encrypted){
        	return password_hash($string, PASSWORD_DEFAULT);
		}else{
			return $string;
		}
    }
    public function connected(){
        $test = 0;
        $login = $this->CI->session->userdata("login");
        //ajouter un get password
        $arrayCollector = $this->CI->Comics_model->comics_getOneCollector($login);
        //verifier en plus le password
        if(!empty($arrayCollector)){
            $test=1;
        }
        return $test;
    }
    public function getConnected($login,$password){ //verifie si ID ok et connecte 
        $test = 0;
        $arrayCollector = $this->CI->Comics_model->comics_getOneCollector($login);
        if($arrayCollector!=null){
            if($this->verify_password($password,$arrayCollector["password"])){
                $test=1;
                $this->CI->session->set_userdata("login",$login);
                
            }
        }
        return $test;
    }
    public function isadmin(){
        //verifier si il est connecté via isconnected
        $admin = 0;
        $arrayCollector = $this->CI->Comics_model->comics_getOneCollector($this->CI->session->userdata("login"));
        if($arrayCollector["admin"]=="t"){
            $admin=1;
        }
        return $admin;
    }
    public function isheadmin($login){
            $admin = 0;
            $arrayCollector = $this->CI->Comics_model->comics_getOneCollector($login);
            if($arrayCollector["admin"]=="t"){
                $admin=1;
            }
            return $admin;
    }
    public function inCollection($comic_id){
        $test=1;
        $login = $this->CI->session->userdata("login");
        $comic = $this->CI->Comics_model->getComicForCollectorAndComic($login,$comic_id);
        if(empty($comic)){
            $test=0;
        }
        return $test;
    }
    
}
