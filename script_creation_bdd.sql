DROP SCHEMA IF EXISTS comics CASCADE;
CREATE SCHEMA comics;

SET search_path = comics, pg_catalog;

SET default_with_oids = false;

CREATE TABLE comics._collect (
    comic_id integer NOT NULL,
    collector_login varchar(8) NOT NULL
);

CREATE TABLE comics._collector (
    login varchar(20) NOT NULL,
    name character varying(30) NOT NULL,
    firstname character varying(30) NOT NULL,
    password character varying(65) NOT NULL, --car une fois la chaine hacher, elle fait automatiquement 60 caractères
    admin boolean DEFAULT NULL
);


CREATE TABLE comics._comic (
  comic_id integer NOT NULL,
  serie varchar(30) DEFAULT NULL,
  numero integer DEFAULT NULL,
  date date DEFAULT CURRENT_DATE,
  couverture varchar(70) DEFAULT NULL
);

--
-- Déchargement des données de la table `comic`
--

INSERT INTO comics._comic (comic_id, serie, numero, date, couverture) VALUES
(1, 'Uncanny X-Men (1963 - 2011)', 1, '1963-09-10', 'http://i.annihil.us/u/prod/marvel/i/mg/c/f0/589ddfc222e9c.jpg'),
(2, 'Uncanny X-Men (1963 - 2011)', 2, '1963-11-01', 'http://i.annihil.us/u/prod/marvel/i/mg/2/c0/589de0deb7afc.jpg'),
(3, 'Uncanny X-Men (1963 - 2011)', 3, '1964-01-01', 'http://i.annihil.us/u/prod/marvel/i/mg/c/60/589de2c6a0bb6.jpg'),
(4, 'Uncanny X-Men (1963 - 2011)', 4, '1964-03-01', 'http://i.annihil.us/u/prod/marvel/i/mg/d/60/589de3be4c9d8.jpg'),
(5, 'Uncanny X-Men (1963 - 2011)', 5, '1964-05-01', 'http://i.annihil.us/u/prod/marvel/i/mg/4/30/589de49c80029.jpg'),
(6, 'Uncanny X-Men (1963 - 2011)', 6, '1964-07-01', 'http://i.annihil.us/u/prod/marvel/i/mg/a/c0/589de6a985d8c.jpg'),
(7, 'Uncanny X-Men (1963 - 2011)', 7, '1964-09-01', 'http://i.annihil.us/u/prod/marvel/i/mg/7/50/589de7d53f750.jpg'),
(8, 'Uncanny X-Men (1963 - 2011)', 8, '1964-11-01', 'http://i.annihil.us/u/prod/marvel/i/mg/d/10/589de8ee8dc00.jpg'),
(9, 'Uncanny X-Men (1963 - 2011)', 9, '1965-01-01', 'http://i.annihil.us/u/prod/marvel/i/mg/d/20/589de9b9a4aa7.jpg'),
(10, 'Uncanny X-Men (1963 - 2011)', 10, '1965-03-01', 'http://i.annihil.us/u/prod/marvel/i/mg/6/c0/590783cee643a.jpg'),
(11, 'Uncanny X-Men (1963 - 2011)', 11, '1965-05-01', 'http://i.annihil.us/u/prod/marvel/i/mg/9/40/4d49e144c4e0d.jpg'),
(12, 'Uncanny X-Men (1963 - 2011)', 12, '1965-07-01', 'http://i.annihil.us/u/prod/marvel/i/mg/6/90/4d49e226c7d51.jpg'),
(13, 'Uncanny X-Men (1963 - 2011)', 13, '1965-09-01', 'http://i.annihil.us/u/prod/marvel/i/mg/9/c0/4d49e19e06f5a.jpg'),
(14, 'Uncanny X-Men (1963 - 2011)', 14, '1965-11-01', 'http://i.annihil.us/u/prod/marvel/i/mg/c/90/4d49e28fe2a3c.jpg'),
(15, 'Uncanny X-Men (1963 - 2011)', 15, '1965-12-01', 'http://i.annihil.us/u/prod/marvel/i/mg/9/90/4d49e09b91dd1.jpg'),
(16, 'Uncanny X-Men (1963 - 2011)', 16, '1966-01-01', 'http://i.annihil.us/u/prod/marvel/i/mg/9/80/4d49e11f8e30e.jpg'),
(17, 'Uncanny X-Men (1963 - 2011)', 17, '1966-02-01', 'http://i.annihil.us/u/prod/marvel/i/mg/a/60/589b6da28f3f9.jpg'),
(18, 'Uncanny X-Men (1963 - 2011)', 18, '1966-03-01', 'http://i.annihil.us/u/prod/marvel/i/mg/1/f0/589b8483e9890.jpg'),
(19, 'Uncanny X-Men (1963 - 2011)', 19, '1966-04-01', 'http://i.annihil.us/u/prod/marvel/i/mg/4/04/589b8ecd5e3ee.jpg'),
(20, 'Uncanny X-Men (1963 - 2011)', 20, '1966-05-01', 'http://i.annihil.us/u/prod/marvel/i/mg/a/90/589c789dd2ff1.jpg'),
(21, 'Uncanny X-Men (1963 - 2011)', 21, '1966-06-01', 'http://i.annihil.us/u/prod/marvel/i/mg/3/60/58a20e65c1179.jpg'),
(22, 'Uncanny X-Men (1963 - 2011)', 22, '1966-07-01', 'http://i.annihil.us/u/prod/marvel/i/mg/2/e0/58a20f540551c.jpg'),
(23, 'Uncanny X-Men (1963 - 2011)', 23, '1966-08-01', 'http://i.annihil.us/u/prod/marvel/i/mg/3/e0/58a21f53872e8.jpg'),
(24, 'Uncanny X-Men (1963 - 2011)', 24, '1966-09-01', 'http://i.annihil.us/u/prod/marvel/i/mg/2/d0/58a220a71b870.jpg'),
(25, 'Uncanny X-Men (1963 - 2011)', 25, '1966-10-01', 'http://i.annihil.us/u/prod/marvel/i/mg/2/f0/58a22156b34f1.jpg'),
(26, 'Uncanny X-Men (1963 - 2011)', 26, '1966-11-01', 'http://i.annihil.us/u/prod/marvel/i/mg/3/00/58a222bd93324.jpg'),
(27, 'Uncanny X-Men (1963 - 2011)', 27, '1966-12-01', 'http://i.annihil.us/u/prod/marvel/i/mg/d/e0/58a2237195576.jpg'),
(28, 'Uncanny X-Men (1963 - 2011)', 28, '1967-01-01', 'http://i.annihil.us/u/prod/marvel/i/mg/3/a0/58a30d7b76545.jpg'),
(29, 'Uncanny X-Men (1963 - 2011)', 29, '1967-02-01', 'http://i.annihil.us/u/prod/marvel/i/mg/6/b0/58a30e694e3d5.jpg'),
(30, 'Uncanny X-Men (1963 - 2011)', 30, '1967-03-01', 'http://i.annihil.us/u/prod/marvel/i/mg/6/60/58a30f5c962c2.jpg'),
(31, 'Uncanny X-Men (1963 - 2011)', 31, '1967-04-01', 'http://i.annihil.us/u/prod/marvel/i/mg/9/30/58a1bfd03474e.jpg'),
(32, 'Uncanny X-Men (1963 - 2011)', 32, '1967-05-01', 'http://i.annihil.us/u/prod/marvel/i/mg/6/60/58a1c07fb6761.jpg'),
(33, 'Uncanny X-Men (1963 - 2011)', 33, '1967-06-10', 'http://i.annihil.us/u/prod/marvel/i/mg/3/b0/5372d274ae693.jpg'),
(34, 'Uncanny X-Men (1963 - 2011)', 34, '1967-07-01', 'http://i.annihil.us/u/prod/marvel/i/mg/6/50/5372d1fe355f0.jpg'),
(35, 'Uncanny X-Men (1963 - 2011)', 35, '1967-08-01', 'http://i.annihil.us/u/prod/marvel/i/mg/8/70/5372cd0d114bb.jpg'),
(36, 'Uncanny X-Men (1963 - 2011)', 36, '1967-09-01', 'http://i.annihil.us/u/prod/marvel/i/mg/8/b0/5372cc62be11b.jpg'),
(37, 'Uncanny X-Men (1963 - 2011)', 37, '1967-10-01', 'http://i.annihil.us/u/prod/marvel/i/mg/e/03/5372cc3575459.jpg'),
(38, 'Uncanny X-Men (1963 - 2011)', 38, '1967-11-01', 'http://i.annihil.us/u/prod/marvel/i/mg/6/60/5372cbe08b628.jpg'),
(39, 'Uncanny X-Men (1963 - 2011)', 39, '1967-12-01', 'http://i.annihil.us/u/prod/marvel/i/mg/c/70/5372cb587cbcd.jpg'),
(40, 'Uncanny X-Men (1963 - 2011)', 40, '1968-01-01', 'http://i.annihil.us/u/prod/marvel/i/mg/2/e0/5372cb0164b46.jpg'),
(41, 'Uncanny X-Men (1963 - 2011)', 41, '1968-02-01', 'http://i.annihil.us/u/prod/marvel/i/mg/b/c0/5372cad6d21cb.jpg'),
(42, 'Uncanny X-Men (1963 - 2011)', 42, '1968-03-01', 'http://i.annihil.us/u/prod/marvel/i/mg/3/70/5372ca84416c7.jpg'),
(43, 'Uncanny X-Men (1963 - 2011)', 43, '1968-04-01', 'http://i.annihil.us/u/prod/marvel/i/mg/6/f0/5372c9fa5d098.jpg'),
(44, 'Uncanny X-Men (1963 - 2011)', 44, '1968-05-01', 'http://i.annihil.us/u/prod/marvel/i/mg/3/80/5372c9c72e315.jpg'),
(45, 'Uncanny X-Men (1963 - 2011)', 45, '1968-06-01', 'http://i.annihil.us/u/prod/marvel/i/mg/6/a0/5372c77c83d3b.jpg'),
(46, 'Uncanny X-Men (1963 - 2011)', 46, '1968-07-01', 'http://i.annihil.us/u/prod/marvel/i/mg/3/60/5372c6e4171d5.jpg'),
(47, 'Uncanny X-Men (1963 - 2011)', 47, '1968-08-01', 'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5372c6b1a9523.jpg'),
(48, 'Uncanny X-Men (1963 - 2011)', 48, '1968-09-01', 'http://i.annihil.us/u/prod/marvel/i/mg/3/03/5372c62361c36.jpg'),
(49, 'Uncanny X-Men (1963 - 2011)', 49, '1968-10-01', 'http://i.annihil.us/u/prod/marvel/i/mg/9/03/5372c5f168190.jpg'),
(50, 'Uncanny X-Men (1963 - 2011)', 50, '1968-11-01', 'http://i.annihil.us/u/prod/marvel/i/mg/4/20/5372c556be877.jpg'),
(51, 'Uncanny X-Men (1963 - 2011)', 51, '1968-12-01', 'http://i.annihil.us/u/prod/marvel/i/mg/3/50/5372c524da628.jpg'),
(52, 'Uncanny X-Men (1963 - 2011)', 52, '1969-01-01', 'http://i.annihil.us/u/prod/marvel/i/mg/6/90/5372c484cf36b.jpg'),
(53, 'Uncanny X-Men (1963 - 2011)', 53, '1969-02-01', 'http://i.annihil.us/u/prod/marvel/i/mg/f/80/5372c453030d0.jpg'),
(54, 'Uncanny X-Men (1963 - 2011)', 54, '1969-03-10', 'http://i.annihil.us/u/prod/marvel/i/mg/3/90/5372c3e7c7137.jpg'),
(55, 'Uncanny X-Men (1963 - 2011)', 55, '1969-04-10', 'http://i.annihil.us/u/prod/marvel/i/mg/7/00/5372bf5639e9a.jpg'),
(56, 'Uncanny X-Men (1963 - 2011)', 56, '1969-05-10', 'http://i.annihil.us/u/prod/marvel/i/mg/e/80/5372bebcbcb30.jpg'),
(57, 'Uncanny X-Men (1963 - 2011)', 57, '1969-06-10', 'http://i.annihil.us/u/prod/marvel/i/mg/9/90/5372be8a9b72c.jpg'),
(58, 'Uncanny X-Men (1963 - 2011)', 58, '1969-07-10', 'http://i.annihil.us/u/prod/marvel/i/mg/c/f0/58a48efbd28a8.jpg'),
(59, 'Uncanny X-Men (1963 - 2011)', 59, '1969-08-10', 'http://i.annihil.us/u/prod/marvel/i/mg/9/70/58a4903ce3c6d.jpg'),
(60, 'Uncanny X-Men (1963 - 2011)', 60, '1969-09-10', 'http://i.annihil.us/u/prod/marvel/i/mg/6/50/58a4917b22c3e.jpg'),
(61, 'Uncanny X-Men (1963 - 2011)', 61, '1969-10-10', 'http://i.annihil.us/u/prod/marvel/i/mg/8/03/58a492438b063.jpg'),
(62, 'Uncanny X-Men (1963 - 2011)', 62, '1969-11-10', 'http://i.annihil.us/u/prod/marvel/i/mg/6/e0/58a493069ae5e.jpg'),
(63, 'Uncanny X-Men (1963 - 2011)', 63, '1969-12-10', 'http://i.annihil.us/u/prod/marvel/i/mg/7/00/58a4945fed480.jpg'),
(64, 'Uncanny X-Men (1963 - 2011)', 64, '1970-01-10', 'http://i.annihil.us/u/prod/marvel/i/mg/f/80/58a4a8ac9cb8b.jpg'),
(65, 'Uncanny X-Men (1963 - 2011)', 65, '1970-02-10', 'http://i.annihil.us/u/prod/marvel/i/mg/6/d0/58a4a9af76561.jpg'),
(66, 'Uncanny X-Men (1963 - 2011)', 66, '1970-03-10', 'http://i.annihil.us/u/prod/marvel/i/mg/b/c0/58a4aaa7b21d6.jpg'),
(67, 'Uncanny X-Men (1963 - 2011)', 67, '1970-12-10', 'http://i.annihil.us/u/prod/marvel/i/mg/a/50/4f67bce2789b7.jpg'),
(68, 'Uncanny X-Men (1963 - 2011)', 68, '1971-02-10', 'http://i.annihil.us/u/prod/marvel/i/mg/6/40/4f67abd521481.jpg'),
(69, 'Uncanny X-Men (1963 - 2011)', 69, '1971-04-10', 'http://i.annihil.us/u/prod/marvel/i/mg/6/50/4f67a31a238e5.jpg'),
(70, 'Uncanny X-Men (1963 - 2011)', 70, '1971-06-10', 'http://i.annihil.us/u/prod/marvel/i/mg/8/f0/4f6740aa9a3b2.jpg'),
(71, 'Uncanny X-Men (1963 - 2011)', 71, '1971-08-10', 'http://i.annihil.us/u/prod/marvel/i/mg/c/10/4f67bb8be22a1.jpg'),
(72, 'Uncanny X-Men (1963 - 2011)', 72, '1971-10-10', 'http://i.annihil.us/u/prod/marvel/i/mg/c/e0/4f67a3e1ee48f.jpg'),
(73, 'Uncanny X-Men (1963 - 2011)', 73, '1971-12-10', 'http://i.annihil.us/u/prod/marvel/i/mg/9/a0/4f67bbd439f22.jpg'),
(74, 'Uncanny X-Men (1963 - 2011)', 74, '1972-02-10', 'http://i.annihil.us/u/prod/marvel/i/mg/c/70/4f67bad153eb8.jpg'),
(75, 'Uncanny X-Men (1963 - 2011)', 75, '1972-04-10', 'http://i.annihil.us/u/prod/marvel/i/mg/c/b0/4f67a8dff0e23.jpg'),
(76, 'Uncanny X-Men (1963 - 2011)', 76, '1972-06-10', 'http://i.annihil.us/u/prod/marvel/i/mg/a/80/4f67b9cf6c199.jpg'),
(77, 'Uncanny X-Men (1963 - 2011)', 77, '1972-08-10', 'http://i.annihil.us/u/prod/marvel/i/mg/6/e0/4f67b9830f53b.jpg'),
(78, 'Uncanny X-Men (1963 - 2011)', 78, '1972-10-10', 'http://i.annihil.us/u/prod/marvel/i/mg/4/90/4f6887ff81057.jpg'),
(79, 'Uncanny X-Men (1963 - 2011)', 79, '1972-12-10', 'http://i.annihil.us/u/prod/marvel/i/mg/1/20/4f688786234c0.jpg'),
(80, 'Uncanny X-Men (1963 - 2011)', 80, '1973-02-10', 'http://i.annihil.us/u/prod/marvel/i/mg/6/f0/4f67b0c675d57.jpg'),
(81, 'Uncanny X-Men (1963 - 2011)', 81, '1973-04-10', 'http://i.annihil.us/u/prod/marvel/i/mg/6/f0/4f5e860e6f056.jpg'),
(82, 'Uncanny X-Men (1963 - 2011)', 82, '1973-06-10', 'http://i.annihil.us/u/prod/marvel/i/mg/3/30/4f5e867bdeffb.jpg'),
(83, 'Uncanny X-Men (1963 - 2011)', 83, '1973-08-10', 'http://i.annihil.us/u/prod/marvel/i/mg/a/00/4f5e85be9a391.jpg'),
(84, 'Uncanny X-Men (1963 - 2011)', 84, '1973-10-10', 'http://i.annihil.us/u/prod/marvel/i/mg/4/90/4f67b6488558c.jpg'),
(85, 'Uncanny X-Men (1963 - 2011)', 85, '1973-12-10', 'http://i.annihil.us/u/prod/marvel/i/mg/1/80/4f67a86841695.jpg'),
(86, 'Uncanny X-Men (1963 - 2011)', 86, '1974-02-10', 'http://i.annihil.us/u/prod/marvel/i/mg/9/90/4f67ae0093a8f.jpg'),
(87, 'Uncanny X-Men (1963 - 2011)', 87, '1974-04-10', 'http://i.annihil.us/u/prod/marvel/i/mg/2/a0/4f67affb17dbe.jpg'),
(88, 'Uncanny X-Men (1963 - 2011)', 88, '1974-06-10', 'http://i.annihil.us/u/prod/marvel/i/mg/f/50/4f67bd7de4b0a.jpg'),
(89, 'Uncanny X-Men (1963 - 2011)', 89, '1974-08-10', 'http://i.annihil.us/u/prod/marvel/i/mg/1/f0/4f67afaa0c0fe.jpg'),
(90, 'Uncanny X-Men (1963 - 2011)', 90, '1974-10-10', 'http://i.annihil.us/u/prod/marvel/i/mg/c/80/4f68b2e8ab9a1.jpg'),
(91, 'Uncanny X-Men (1963 - 2011)', 91, '1974-12-10', 'http://i.annihil.us/u/prod/marvel/i/mg/3/80/4f68b2176237a.jpg'),
(92, 'Uncanny X-Men (1963 - 2011)', 92, '1975-02-10', 'http://i.annihil.us/u/prod/marvel/i/mg/5/b0/4f68b190a0a22.jpg'),
(93, 'Uncanny X-Men (1963 - 2011)', 93, '1975-06-10', 'http://i.annihil.us/u/prod/marvel/i/mg/6/c0/4f68b38a0259e.jpg'),
(94, 'Uncanny X-Men (1963 - 2011)', 94, '1975-08-10', 'http://i.annihil.us/u/prod/marvel/i/mg/3/50/58a60168b290d.jpg'),
(95, 'Uncanny X-Men (1963 - 2011)', 95, '1975-10-10', 'http://i.annihil.us/u/prod/marvel/i/mg/4/20/58a60348b8f59.jpg'),
(96, 'Uncanny X-Men (1963 - 2011)', 96, '1975-12-10', 'http://i.annihil.us/u/prod/marvel/i/mg/2/f0/58a604a4a5425.jpg'),
(97, 'Uncanny X-Men (1963 - 2011)', 97, '1976-02-10', 'http://i.annihil.us/u/prod/marvel/i/mg/3/d0/58a605925fe17.jpg'),
(98, 'Uncanny X-Men (1963 - 2011)', 189, '1985-01-01', 'http://i.annihil.us/u/prod/marvel/i/mg/6/00/590350adf3a82.jpg'),
(99, 'Uncanny X-Men (1963 - 2011)', 190, '1985-02-10', 'http://i.annihil.us/u/prod/marvel/i/mg/1/80/59035298d7a99.jpg'),
(100, 'Uncanny X-Men (1963 - 2011)', 191, '1985-03-10', 'http://i.annihil.us/u/prod/marvel/i/mg/3/70/51117ed93d428.jpg');

ALTER TABLE ONLY comics._collect
    ADD CONSTRAINT _collect_pkey PRIMARY KEY (comic_id, collector_login);
    
ALTER TABLE ONLY comics._collector
    ADD CONSTRAINT _collector_pkey PRIMARY KEY (login);
    
ALTER TABLE ONLY comics._comic
    ADD CONSTRAINT _comic_pkey PRIMARY KEY (comic_id);

ALTER TABLE ONLY comics._collect
    ADD CONSTRAINT _collect_fk_collector FOREIGN KEY (collector_login) REFERENCES comics._collector(login) ON DELETE CASCADE;

ALTER TABLE ONLY comics._collect
    ADD CONSTRAINT _collect_fk_comic FOREIGN KEY (comic_id) REFERENCES comics._comic(comic_id);
    

INSERT INTO comics._collector (login,name,firstname,password,admin) VALUES
('Admin','NameAdmin','FirstNameAdmin','password',true),
('User','NameUser','FirstNameUser','password',false);

INSERT INTO comics._collect (comic_id,collector_login) VALUES
(1,'User'),
(2,'User'),
(5,'User'),
(7,'User'),
(10,'User'),
(99,'User'),
(100,'User'),
(91,'User');

