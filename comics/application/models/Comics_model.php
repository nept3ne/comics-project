<?php
class Comics_model extends CI_Model{
	public function __construct(){
		$this->load->database();
		$this->load->library('Etat');
	}
//------------------------Getters function------------------------------
	public function comics_getOneCollector($login){//Obtient 1 seul collector en fonction du login
		$this->db->select('*');
		$this->db->from('_collector');
		$this->db->where(array('login'=>$login));
		$query = $this->db->get()->row_array();
		return $query;
	}
	public function getOneComic($comic_id){
		return $this->db->get_where("_comic",array("comic_id"=>$comic_id))->result_array()[0];
	}
	public function getComicForCollectorAndComic($login,$comic_id){
		$this->db->from('_collect');
		$this->db->where(array("collector_login"=>$login,"comic_id"=>$comic_id));
		return $this->db->get()->result_array();
	}
	public function getNumberOfComics(){
		return $this->db->count_all('_comic');
	}
	public function getTenComics($nb){
		if($nb<1){
			$nb=0;
		}else{
			$nb--;
		}
		return $this->db->get('_comic',10,$nb*10)->result_array();
	}
  //-----getAll

	public function getAllCollector(){
		return $this->db->get('_collector')->result_array();
	}
	public function getAllCollectForCollector($login){
		return $this->db->get_where("_collect",array("collector_login"=>$login))->result_array();
	}
	public function getAllComicForCollector($login){
		$array = $this->getAllCollectForCollector($login);
		$returnArray = array();
		foreach ($array as $element) {
			$arrayElement = $this->getOneComic($element["comic_id"]);
			
			$element["serie"]=$arrayElement["serie"];
			$element["numero"]=$arrayElement["numero"];
			$element["date"]=$arrayElement["date"];
			$element["couverture"]=$arrayElement["couverture"];
			$returnArray[]=$element;
		}
		return $returnArray;
	}
	
//------------------------Setters function--------------------------
	public function setAdmin($login,$isadmin){
		
		if($isadmin==0){
			$isadmin="f";
		}else{
			$isadmin="t";
		}
		$this->db->where('login',$login);
		$this->db->update('_collector',array("admin"=>$isadmin));
	}
	public function setPassword($login,$password){
		$this->db->where('login',$login);
		$this->db->update('_collector',array("password"=>$password));
	}
	public function setCollector($login,$name,$firstname,$password){
		$this->db->where('login',$login);
		$this->db->update('_collector',array("name"=>$name,"firstname"=>$firstname,"password"=>$this->etat->encryption($password)));
	}
	public function setCollectorExceptPass($login,$name,$firstname){
		$this->db->where('login',$login);
		$this->db->update('_collector',array("name"=>$name,"firstname"=>$firstname));
	}
//------------------------Add function-------------------------------
	public function addCollector($login,$name,$firstname,$password){
		$data = array(
			'login'=>$login,// login = 'teste'
			'name'=>$name,
			'firstname'=>$firstname,
			'password'=>$password,
			'admin'=>false
		);
		$this->db->insert('_collector',$data);
	}
	public function addCollect($login,$comic_id){
		print_r($this->Comics_model->getAllCollectForCollector($login));
		if(!empty($this->Comics_model->getAllCollectForCollector($login))){
			$arrayComic = $this->Comics_model->getAllCollectForCollector($login);
			$size=sizeof($arrayComic);
			if($size>=10){
				$this->Comics_model->delCollect($login,$arrayComic[9]["comic_id"]);

			}
		}else{
			$size=0;
		}
		$data = array(
			'comic_id'=>$comic_id,
			'collector_login'=>$login
		);
		$this->db->insert('_collect',$data);
		return $size;
	}	
	
	
	
//--------------------------Delete function---------------------------
	public function delCollector($login){
	// la suppression automatique de la collection des collecteurs
		$data = array('login'=>$login);
		$this->db->where($data);
		$this->db->delete('_collector');
	}
	public function delCollect($login,$comic_id){
		$data = array(
			'collector_login'=>$login,
			'comic_id'=>$comic_id
		);
		$this->db->delete('_collect',$data);
	}
}
?>
