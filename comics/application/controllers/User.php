<?php
    class User extends CI_Controller{
        public function __construct(){
            parent::__construct();
            //chargement des models,librairies,helper
            $this->load->model("Comics_model");
            $this->load->library('session');
            $this->load->library('etat');
            $this->load->library('Result');
            $this->load->helper('url');
            $this->load->library('form_validation');
            //verification, si l'utilisateur est connecté
            if(!$this->etat->connected()==1){
                $this->session->set_userdata("error","discUser");
                redirect("comics/connexion");
            }
			
        }

        public function index(){
            //login -> type : session type : session,
            //affiche la page d'accueil de l'utilisateur

            $login = $this->session->userdata("login");
            $arrayCollector = $this->Comics_model->comics_getOneCollector($login);
            $data = array(
                "name"=>$arrayCollector["name"],
                "firstname"=>$arrayCollector["firstname"],
                "listeComic"=>$this->Comics_model->getAllComicForCollector($login)
            );
            $dataTemplate = array(
                "done"=>$this->result->inDone(),
                "error"=>$this->result->inError(),
                "view1"=>"profile",
                "data1"=>$data
            );
            $this->load->view("template",$dataTemplate);
        }
        public function set(){
            //oldPassword -> type : post necessite forcement l'ancien mdp
            //name -> type : post
            //firstname -> type : post
            //password -> type : post
            //passwordConf -> type : post
            //login -> type : session
            //permet de modifier les info de l'utilisateur

            $this->form_validation->set_rules('oldPassword','erreur de saisie','required');
            if($this->form_validation->run()===TRUE){
                $login = $this->session->userdata("login");
                $arrayCollector = $this->Comics_model->comics_getOneCollector($login);
                $oldpassword = $this->security->xss_clean($this->input->post("oldPassword"));

                $name = $this->security->xss_clean($this->input->post("name"));
                if($name==NULL){$name=$arrayCollector["name"];}
                $firstname = $this->security->xss_clean($this->input->post("firstname"));
                if($firstname==NULL){$firstname=$arrayCollector["firstname"];}
                $password = $this->security->xss_clean($this->input->post("password"));
                $passwordConf = $this->security->xss_clean($this->input->post("passwordConf"));
                if($password==null){
                    $password=$oldpassword;
                    $passwordConf=$oldpassword;
                }
            
                
                if($password==$passwordConf){
                    if($this->etat->verify_password($oldpassword,$arrayCollector["password"])){
                        $this->Comics_model->setCollector($login,$name,$firstname,$password);    
                        $this->session->set_userdata("done","accountchanged");
                        redirect("user");
                    }else{
                        $this->session->set_userdata("error","iderror");
                        redirect("user/set");
                    }
                }else{
                    $this->session->set_userdata("error","mdpincorconf");
                    $array["adminMode"] = FALSE;
                    $data = array(
                        "error"=>$this->result->inError(),
                        "view1"=>"modifierCompte",
                        "data1"=>$array
                    );
                    $this->load->view("template",$data);
                }


                
            } else{
                $array = $this->Comics_model->comics_getOneCollector($this->session->userdata("login"));
                $array["adminMode"] = FALSE;
                $dataTemplate = array(
                    "done"=>$this->result->inDone(),
                    "error"=>$this->result->inError(),
                    "view1"=>"modifierCompte",
                    "data1"=>$array
                );
                $this->load->view("template",$dataTemplate);
            }
    }

        public function del(){
            //login : type session
            //supprime l'utilisateur

                $this->Comics_model->delCollector($this->session->userdata("login"));
                $this->session->unset_userdata("login");
                $this->session->set_userdata("done","delUser");//////////////////////case affichage
                redirect("comics");         
        }

        public function deconnexion(){
            //login -> type : session
            //decconnect

                $array_sess = array("login","password","admin");
                $this->session->unset_userdata("login");
                $this->session->unset_userdata("password");
                $this->session->set_userdata("done","discUser");//////////////////////case affichage
                redirect('comics');
        }

        public function lier($comic_id){
            //$comic_id -> obligatoire
            //login -> type : session
            //lie un comic et un utilisateur
            $size = $this->Comics_model->getNumberOfComics(); //merde 1
                if(!$this->etat->inCollection($comic_id)){
                    $comic = $this->Comics_model->getOneComic($comic_id); 
                    if($size==10){
                        $this->session->set_userdata("error","10Coms");//////////////////////case affichage
                    }else{
                        $this->session->set_userdata("done","comAdd");//////////////////////case affichage
                    }
                    $this->Comics_model->addCollect($this->session->userdata("login"),$comic_id); //merde 2
                    $data = array(
                        "error"=>$this->result->inError(),
                        "done"=>$this->result->inDone(),
                        "data1"=>$comic,
                        "view1"=>"unComic"
                    );
                    $this->load->view("template",$data);
                    $url = base_url()."index.php/comics/listeComic";
                    header("refresh:4;url=$url"); //permet de rediriger 4 secondes après le chargement de la page
                }else{
                    $this->session->set_userdata('error',"doubCom");
                    redirect('comics/listeComic/'.$comic_id%10);
                }
        }

        public function supprLien($comic_id){
            //comic_id -> obligatoire
            //login -> type : post
            //supprime un collect

            $login = $this->session->userdata("login");
            $this->Comics_model->delCollect($login,$comic_id);
            $this->session->set_userdata("done","delCom"); //////////////////////case affichage
            redirect("user/index");
    }


    }
?>
