<div class="box">
	<p>
		Voici le site sur le thème des comics, les fonctionnalités sont les suivantes : 
	</p>
	<ul>
		<li>
			Fonctionnalité, si l'internaute est déconnecté 
			<ul>
				<li>Voir la page d'accueil</li>
				<li>Voir la liste des comics (10 comics par page, avec boutons pour tourner les pages)</li>
				<li>Se créer un compte</li>
				<li>S'identifier</li>
			</ul>
		</li>
		<li>
			Fonctionnalités, si l'internaute est connecté
			<ul>
				<li>Voir un récapitulatif de son compte et voir sa collection de comic</li>
				<li>Modifier son compte (tout sauf le pseudo)</li>
				<li>Supprimer son compte (avec fenetre de confirmation en js)</li>
				<li>Ajouter un comic</li>
				<li>Supprimer un comic de sa collection</li>
			</ul>
		</li>
		<li>
			Fonctionnalités si l'internaute est connecté et un admin
			<ul>
				<li>Supprimer un compte</li>
				<li>Modifier un compte d'un autre utilisateur : <ul>
					<li></li>
					<li></li>
					<li></li>
					<li></li>
				</ul></li>
			</ul>
		</li>
	</ul>
</div>
<?php

?>
