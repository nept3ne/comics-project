<div >
    <img src="<?php echo $couverture ; ?>" alt="une image">
    <div>
            <h3>
                Le nom de la série : <?php echo $serie ; ?>
            </h3>
            <h4>
                Le numero : <?php echo $numero ; ?>
            </h4>
            <h4>
                la date : <?php echo $date ; ?>
            </h4>
    </div>
    <?php 
    if(!$this->etat->inCollection($comic_id) && $this->etat->connected()){
        echo '<a href="'.base_url().'index.php/user/lier/'.$comic_id.'"><i class="fas fa-plus-square fa-3x"></i></a>';        
    }else if($this->etat->connected()){
        echo '<a href="'.base_url().'index.php/user/supprLien/'.$comic_id.'"><i class="fas fa-trash fa-3x"></i></a>';    
    }
    ?>
</div>
